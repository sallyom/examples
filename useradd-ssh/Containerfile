# Add an unprivileged user with a ssh key that has passwordless sudo access.
# See also https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_basic_system_settings/managing-sudo-access_configuring-basic-system-settings
# Note that because the user home directories are writable persistent
# state by default under /var, subsequent changes here will NOT
# take effect on in-place updates by default.  This means that if you want to e.g.
# rotate the SSH key, it must be done manually (by writing a systemd unit that performs
# a rotation for example).
#
# Some use cases may actually prefer to use the network for a source of truth for
# SSH keys instead.
#
# Build like this:
#
# podman build --build-arg "sshpubkey=$(cat ~/.ssh/mykey.pub)" -t quay.io/exampleos/example-image .
FROM quay.io/centos-bootc/centos-bootc:stream9
# Enable passwordless sudo for users in the wheel group
COPY wheel-nopasswd /etc/sudoers.d
ARG sshpubkey
# We don't yet ship a one-invocation CLI command to add a user with a SSH key unfortunately
RUN if test -z "$sshpubkey"; then echo "must provide sshpubkey"; exit 1; fi; \
    useradd -G wheel exampleuser && \
    mkdir -m 0700 -p /home/exampleuser/.ssh && \
    echo $sshpubkey > /home/exampleuser/.ssh/authorized_keys && \
    chmod 0600 /home/exampleuser/.ssh/authorized_keys && \
    chown -R exampleuser: /home/exampleuser
