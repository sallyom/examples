### Run text summarizer application locally as a podman pod

There are pre-built images and a pod definition to run the text-summarizer example application.
To run locally, 

```bash
podman kube play ./summarizer.yaml
```
To monitor locally,

```bash
podman pod list
podman ps 
podman logs <name of container from the above>
```

The application should be acessible at `http://localhost:8501`. It will take a few minutes for the model to load. 

### Run summarizer application as a systemd service

```bash
cp summarizer.yaml /etc/containers/systemd/summarizer.yaml
cp summarizer.kube.example /etc/containers/summarizer.kube
cp summarizer.image /etc/containers/summarizer.image
/usr/libexec/podman/quadlet --dryrun (optional)
systemctl daemon-reload
systemctl start summarizer
```
