### Run code-generator chat application locally as a podman pod

There are pre-built images and a pod definition to run the code-generation example application.
To run locally, 

```bash
podman kube play ./codegen.yaml
```
To monitor locally,

```bash
podman pod list
podman ps 
podman logs <name of container from the above>
```

The application should be acessible at `http://localhost:8501`. It will take a few minutes for the model to load. 

### Run code-generator as a systemd service

```bash
cp codegen.yaml /etc/containers/systemd/codegen.yaml
cp codegen.kube.example /etc/containers/codegen.kube
cp codegen.image /etc/containers/codegen.image
/usr/libexec/podman/quadlet --dryrun (optional)
systemctl daemon-reload
systemctl start codegen
```
